# temp and humidity A2308 module for lnxlink

A 3rd party add on module for [lnxlink](https://github.com/bkbilly/lnxlink) to use an AM2302 temperature and humidity sensor

Requires [Adafruit_CircuitPython_DHT](https://github.com/adafruit/Adafruit_CircuitPython_DHT)

## Install

- Install lnxlink as described https://bkbilly.gitbook.io/lnxlink
- install circuitpython DHT with pip `pip3 install adafruit-circuitpython-dht`
- Install sysv-ipc especially if you have a slow pi `pip3 install sysv-ipc`
- copy the temphumid.py file to `lnxlink/modules`


