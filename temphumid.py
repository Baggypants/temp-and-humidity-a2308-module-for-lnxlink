import time
import board
import adafruit_dht

dhtDevice = adafruit_dht.DHT22(board.D4)

class Addon():

    """Addon module"""

    def __init__(self, lnxlink):
        """Setup addon"""
        self.name = "Temperature and Humidity"

    def exposed_controls(self):
        """Exposes to home assistant"""
        return {
            "Temperature C": {
                "type": "sensor",
                "unit": "°C",
                "state_class": "measurement",
                "device_class": "temperature",
                "value_template": "{{ value_json.temperature_c }}",
            },
            "Humidity": {
                "type": "sensor",
                "unit": "%",
                "state_class": "measurement",
                "device_class": "humidity",
                "value_template": "{{ value_json.humidity }}",
            }
        }


    def get_info(self):
        """Gather information from the system"""
        while True:
            try:
                #temperature_c = dhtDevice.temperature
                #humidity = dhtDevice.humidity
                return {
                    "temperature_c": dhtDevice.temperature, "humidity": dhtDevice.humidity
                }
            except RuntimeError as error:
                # Errors happen fairly often, DHT's are hard to read, just keep going
                continue
